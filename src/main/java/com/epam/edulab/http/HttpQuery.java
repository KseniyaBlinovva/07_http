package com.epam.edulab.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpQuery {
    public String  getCordinates() throws IOException {
        String string = getRequest();
        String cordinate = null;
        Pattern pattern2 = Pattern.compile("(?<=coordinates\":\\[).*?(?=])");
        Matcher matcher2 = pattern2.matcher(string);
        while (matcher2.find()) {
            cordinate = matcher2.group();
            break;
        }
        String[] coordinates = cordinate.split(",");
        return coordinates[1] + "," + coordinates[0];
    }

    public String getRequest() throws IOException {
        String parametrs = getParametrs();
        String token = getToken(parametrs);
        String yandexuid = getYandexuid(parametrs);
        URL url = new URL(String.format("https://yandex.ru/maps/api/search?text=Ижевск,Карла+Маркса,246&lang=ru_RU&csrfToken=%s", token));
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(String.valueOf(url));
        request.addHeader("Cookie", "yandexuid="+yandexuid);
        HttpResponse response;
        response = client.execute(request);
        System.out.println(response);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String answer;
        String input;
        StringBuffer st = new StringBuffer();
        while ((input = bufferedReader.readLine()) != null) {
            st.append(input);
        }
        answer = st.toString();
        System.out.println("answer: " + answer);
        return answer;

    }

    public String getParametrs() throws IOException {

        URL get = new URL("https://yandex.ru/maps");
        HttpURLConnection connection = (HttpURLConnection) get.openConnection();
        connection.setRequestMethod("GET");
        String str;
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
        InputStream is = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String input;
        StringBuffer response = new StringBuffer();

        while ((input = bufferedReader.readLine()) != null) {
            response.append(input);
        }
        str = response.toString();
        bufferedReader.close();
        return str;
    }

    public String getToken(String str) {
        String csrfToken = null;
        Pattern pattern = Pattern.compile("(?<=csrfToken\":\").*?(?=\")");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            csrfToken = matcher.group();
            System.out.println("csrfToken: " + csrfToken);
        }
        return csrfToken;
    }

    public String getYandexuid(String str) {
        String yandexuid = null;
        Pattern pattern1 = Pattern.compile("(?<=yandexuid=).*?(?=\")");
        Matcher matcher1 = pattern1.matcher(str);
        if (matcher1.find()) {
            yandexuid = matcher1.group();
            System.out.println("yandexuid: " + yandexuid);
        }
        return yandexuid;
    }

}